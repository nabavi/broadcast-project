package com.example.mustafanabavi.broadcust;

import android.content.Context;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telecom.ConnectionService;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button btn_enable, btn_desible;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_enable = (Button)findViewById(R.id.button_start);
        btn_desible=(Button)findViewById(R.id.button_off);

        btn_enable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
                wifi.setWifiEnabled(true);
                wifi.startScan();



            }
        });


        btn_desible.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                WifiManager wf = (WifiManager)getSystemService(Context.WIFI_SERVICE);
                wf.setWifiEnabled(false);
                String text = getWifiName(getApplicationContext());
                Toast.makeText(getApplicationContext(),"The wifi name is :"+ text,Toast.LENGTH_SHORT).show();

            }
        });


    }

    public String getWifiName(Context context) {
        WifiManager manager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if (manager.isWifiEnabled()) {
            WifiInfo wifiInfo = manager.getConnectionInfo();
            if (wifiInfo != null) {
                NetworkInfo.DetailedState state = WifiInfo.getDetailedStateOf(wifiInfo.getSupplicantState());
                if (state == NetworkInfo.DetailedState.CONNECTED || state == NetworkInfo.DetailedState.OBTAINING_IPADDR) {
                    return wifiInfo.getSSID();
                }
            }
        }
        return null;
    }
}
